package ru.rt;

public class Main {

    public static void main(String[] args) {
        int size = 25;
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                System.out.print(
                        x == y || x == 24 - y
                        ? "# " : ". ");
            }
            System.out.println();
        }
    }
}

//1. x < y
//2. x == y
//3. x == 24-y
//4. x + y < 30
//6. x<10 || y<10
//7. x>15 && y>15
//8. x*y==0
//14. x * y < 100
//18. x * y < x + y
//19. x * y == 0 || x == 24 || y == 24
//20. (x + y) % 2 == 0
//22. (x + y) % 3 == 0
//24. x == y || x == (24 - y)
